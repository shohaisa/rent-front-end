export const statuses = [
  {
    value: 'not_started',
    label: 'Новый',
  },
  {
    value: 'in_work',
    label: 'В работе',
  },
  {
    value: 'completed',
    label: 'Завершен',
  },
  {
    value: 'not_paid',
    label: 'Не оплачено',
  },
  {
    value: 'closed',
    label: 'Закрыто',
  },
]
export const statusesMap = {}
for (const item of statuses) {
  statusesMap[item.value] = item.label
}
