import formatter from 'dateformat'

export default (date, format = 'dd.mm.yyyy hh:MM') =>
  date ? formatter(date, format) : date
